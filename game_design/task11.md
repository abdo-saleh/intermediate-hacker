MatrixSSL is an embedded SSL and TLS solution for IoT devices with a compact
footprint and minimal per-connection overhead. TLS 1.3 client and server support,
mutual authentication, session resumption, and implementations of RSA, ECC,
AES, SHA1, SHA-256, ChaCha20-Poly1305 and other cryptographic algorithms
are all included. The source code is fully documented, and it includes portability
layers for other operating systems, cipher suites, and cryptography providers.
One of its features is to validate a public X.509 certificate, but a stack buffer
overflow is happening in MatrixSSL.

The flag of this task is the function name
where the stack overflow is occuring along side with its CVE id number