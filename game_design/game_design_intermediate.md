# Intermediate hacker training

Welcome! You are a good hacker, you have passed through tough moments at school, and you had interesting classes about information security topics, you missed some classes but you decided to learn alone, you got a good experience in using Kali Linux tools, and now it's time for you to show it, they told you we have laporatory at the school with a server running some critical services, and they feel that they need to check its level of secuirty. you will be kind to help them?

## Disclaimer

This game is for educational purposes only. Unauthorized use of the tools that are incorporated into the game is illegal. The story is fictitious.

## License

This work by Abd alrahman saleh is licensed under CC BY 4.0. Terms of this license are available at: https://creativecommons.org/licenses/by/4.0

## Content

## Collecting infomration - reconnaissance 

## Level 1: 
**Learning outcomes**:

The main goal of this task is to learn how to start collecting information about
the current network, how to scan the connected devices in order finding out the
proper target.

**Task assignment**:
One of the most interesting tools in Kali Linux is NMAP, Nmap may be character-
ized as a tool that can identify or troubleshoot services operating on a machine
connected to the Internet at a high level. Network administrators typically use this
tool to discover possible security problems in their own network systems. It may
also be used to automate a variety of duplicate operations, such as service update
schedule monitoring.

**Flag**: 
The flag for this assignment should be the Nmap command to discover the devices and the IP of the server. 
###     hint ifconfig, nmap,
## Level 2:  
**Learning outcomes**:

The main goal of this task is to use the previously found information (Server
IP, opened ports) as an input to this tool. 

**Task assignment**:
With the popular Nikto Web Scanner, you can scan your website and server right
away. This service may be used to check for known security vulnerabilities and
misconfigurations on a website, virtual host, or web server.


**Flag**:
The flag will be a list of all Nikto commands which are used to scan the running services along side with the found vulnerabilities.

###     hint: nikto
## Level 2: 
**Learning outcomes**:

The main goal of this task is to gain SSH access to the server by brute-forcing
the login. 

**Task assignment**:
Using Metasploit’s auxiliary ssh login module is highly flexible, since it can not
only test a set of credentials over a range of IP addresses, but it can also try brute
force logins.
**Flag**:
The flag will be the username and password for the ssh connection as
well a screenshot of the used module from Metasploit.
## Level 3: 

**Learning outcomes**:

The main goal of this task is to exploit the service. 

**Task assignment**:

The "Very Secure FTP Daemon," or vsftpd, is an FTP server for Unix-like platforms,
including Linux. The GNU General Public License governs its use. IPv6 and SSL
are both supported. In July 2011, it was revealed that the master site’s vsftpd
version 2.3.4 had been corrupted. Users that enter onto a hacked vsftpd-2.3.4 server
with the login ":)" have access to a command shell on port 6200. This was not a
problem with vsftpd’s security; instead, someone had uploaded a different version
of vsftpd that had a backdoor.


**Flag**:
The flag will be the used metasploit module as well as the CVE id number.

## Level 4: 
**Learning outcomes**:
The goal of this task is to learn how to get credientials.

**Task assignment**:
As long as Secure Sockets Layer (SSL) is implemented, many Web sites are con-
sidered "secure." If only it was that easy. Unlike operating system and database
accounts, which are easy to install and enforce strong password constraints, Web
application authentication measures are all over the place. Now we already got
access to the server, so we will start escalating, we found out that a vulnerable web
application is hosted and a login page is prompted. 

**Flag**:
The username and password to login
## Level 5:
**Learning outcomes**:
The goal of this task is to learn how to use Jhon tool. 

**Task assignment**:
John Ripper is a quick password cracker whose main goal is to find weak Unix
passwords. In addition to the hash type of numerous crypt passwords most
typically found in Unix codes, Kerberos / AFS and Windows LM hashes, DES-
based tripcodes, and hundreds of other hashes.
The server has a protected pdf file stored on the desktop under crackme folder,

**Flag**:
The flag will be the Command used to crack and the password of the PDF file.

## Level 6:

**Task assignment**:
MoinMoin 1.9.5 has a vulnerability. The flaw occurs in the way the twikidraw
operations are managed, where a traversal route may be utilized to upload arbitrary
files. Overwriting moin.wsgi on Apached/mod wsgi setups allows for the execution
of arbitrary python code, as shown in the wild in July 2012

**Flag**:
The flag for this task is the CVE ID number, and a screenshot of the exploitation
result.

## Binary - exploiting a binary file

## Level 1: 

**Learning outcomes**:

The first task in buffer overflow is about a binary file which takes a sequence of inputs.

**Task assignment**:
A buffer overflow vulnerability exposes a system to attackers who may take advan-
tage of it by introducing custom code. This kind of malicious programming creates
network security difficulties by causing buffer overflows and placing executable code
in memory areas near to the overflow. The attacker may use this code to launch
other applications or get administrator privileges.

**Flag**:
The flag for this task will be the required payload to cause the buffer
overflow.
## Level 2: 


**Task assignment**:
The Stack is the most often affected by buffer overflows,
which may cause a process to crash, leak information inside the program, or even
compel the process to spawn additional processes. The stack is a kind of memory
buffer that is used to store data that is now being used by the process or will be
required in the future.

The second task of binary exploiting is about an overflow occurring on the
stack, the goal of this task will be altering the return address. To take advantage
of the buffer issue, we want to modify the return address to someplace we can put
some code that, when run, will do something useful for us as an attacker, such as
starting a shell.

**Flag**:
The flag for this task is the Payload which is used to execute a shell code.

## Level 3:  
**Task assignment**:
The heap is a counterpart to the stack that is found within every running program.
Heap memory is also used to preserve information and is used by a running
application, but the heap may grow and shrink in length as needed.
Because of its capacity to allocate and release memory blocks on demand as
process code requires, the heap is commonly referred to as dynamic memory. A
memory heap is a region of memory used to temporarily store data while a program
is executing. The word heap indicates that this area of memory includes a collection
of objects heaped on top of each other. That’s all a memory heap is.
The third task will be a about an overflow happening on the heap, the goal of
this task will be learning how to find the buffer on the heap, and how to exploit it.

**Flag**:
The flag will be the payload used to crash the application.