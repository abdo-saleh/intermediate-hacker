---
# Basic configuration of all defined devices

# Installation of common tools at all hosts
- name: Configuring hosts
  hosts: hosts
  become: true
  roles:
    - hosts

# Installation and configuration of each host
- name: Configuring devices separately
  hosts: hosts
  become: true
  tasks:
  - name: include role
    include_role:
      name: "{{ inventory_hostname }}"

# Enable logging
- hosts: attacker
  become: true
  tasks:
    - name: Set DNS server on attacker
      lineinfile:
        path: /etc/resolv.conf
        line: "nameserver 8.8.8.8"
    - name: Add apt signing key
      apt_key:
        url: https://archive.kali.org/archive-key.asc
        state: present

- hosts: attacker
  become: true
  roles:
      - chrony

- name: setup logging bash
  hosts:
    - attacker
    - server
    - client
  gather_facts: true
  become: true
  become_user: root
  vars:
    student_id: 0
    sandbox_name: junior-hacker-local
    central_syslog_ip: 78.128.251.179
  roles:
    - role: sandbox-logging-forward
      slf_destination: '{% if hostvars["man"] is defined -%} {{ hostvars["man"].ansible_host }} {%- else -%} {{ central_syslog_ip }} {%- endif %}'
      slf_sandbox_id: "{{ kypo_global_sandbox_allocation_unit_id | default(student_id) }}"
      slf_sandbox_name: '{{ kypo_global_sandbox_name | default(sandbox_name) }}'
      slf_pool_id: '{{ kypo_global_pool_id | default(None) }}'
    - role: sandbox-logging-bash

- name: setup logging msf
  hosts:
    - attacker
  gather_facts: true
  become: true
  become_user: root
  roles:
    - role: sandbox-logging-msf

# Install apache on the server
- name: Install apache on the server
  hosts: server
  become: true
  vars_files:
      - roles/server/vars/default.yml
  tasks:
    - name: Install Apache
      apt: name=apache2 update_cache=yes state=latest
    - name: Create document root
      file:
        path: "/var/www/{{ http_host }}"
        state: directory
        owner: "{{ app_user }}"
        mode: '0755'
    # - name: Copy index test page
    #   template:
    #     src: "roles/server/files/index.html.j2"
    #     dest: "/var/www/{{ http_host }}/index.html"

    - name: Set up Apache virtuahHost
      template:
        src: "roles/server/files/apache.conf.j2"
        dest: "/etc/apache2/sites-available/{{ http_conf }}"

    - name: Enable new site
      shell: /usr/sbin/a2ensite {{ http_conf }}
      notify: Reload Apache

    - name: Disable default Apache site
      shell: /usr/sbin/a2dissite 000-default.conf
      when: disable_default
      notify: Reload Apache

  handlers:
    - name: Reload Apache
      service:
        name: apache2
        state: reloaded

- name: Install MariaDB database on the server
  hosts: server
  become: true
  tasks:
    - name: Install MariaDB
      apt: name=mariadb-server state=latest update_cache=yes
    - name: Install php
      apt: name=php state=latest update_cache=yes
    - name: Install php-mysqli
      apt: name=php-mysqli state=latest update_cache=yes
    - name: Install php-gd
      apt: name=php-gd state=latest update_cache=yes
    - name: Install libapache2-mod-php
      apt: name=libapache2-mod-php state=latest update_cache=yes
    - name: Install gcc-multilib
      apt: name=gcc-multilib state=latest update_cache=yes

    - name: Determine required MySQL Python libraries.
      set_fact:
        deb_mysql_python_package: "{% if 'python3' in ansible_python_interpreter|default('') %}python3-mysqldb{% else %}python-mysqldb{% endif %}"

    - name: Ensure MySQL Python libraries are installed.
      apt: "name={{ deb_mysql_python_package }} state=present"

    - name: Set allow_url_include to on 
      become: true
      lineinfile:
        dest: /etc/php/7.3/apache2/php.ini
        regexp: "allow_url_include = "
        line: "allow_url_include = on"
      notify: Restart Apache

    - name: Set allow_url_include to on
      become: true
      lineinfile:
        dest: /etc/php/7.3/apache2/php.ini
        regexp: "allow_url_fopen = "
        line: "allow_url_fopen = on"
      notify: Restart Apache

    - name: Create database user with name 'dvwa' and password 'heslojekreslo' with all database privileges
      mysql_user:
        name: dvwa
        password: heslojekreslo
        priv: '*.*:ALL'
        state: present

    - name: Start the MySQL service
      action: service name=mysql state=started

    - name: Remove the test database
      mysql_db: name=test state=absent

    - name: Create dvwa user for mysql
      mysql_user: user="dvwa" host="%" password=heslojekreslo priv=*.*:ALL,GRANT

    - name: Ensure anonymous users are not in the database
      mysql_user: user='' host=$item state=absent
      with_items:
        - 127.0.0.1
        - ::1
        - localhost
  handlers:
    - name: Restart Apache
      service:
        name: apache2
        state: restarted