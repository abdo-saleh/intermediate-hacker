# Cybersecurity game "Intermediate hacker training"

This cybersecurity game allows a **hands-on demonstration and practice** of topics such as network scanning, SSH connections, , password cracking and binary overflows. It is suitable for undergraduate students in computing.

Please follow the [general instructions](https://gitlab.ics.muni.cz/muni-kypo-trainings/games/all-games-index) to set up the game.

The description about the game and instructions can be found in the game_design/game_design_intermediate.md file.

## Credits

[Cybersecurity Laboratory](https://cybersec.fi.muni.cz)\
Faculty of Informatics\
Masaryk University

**Leading author:** Bc. Abd Alrahman Saleh

**Contributors/Consultants:** Pavol Helebrandt, Ing. PhD
